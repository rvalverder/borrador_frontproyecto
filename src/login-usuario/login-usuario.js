import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div hidden$="{{isLogged}}" >
      <h2>login usuario</h2>
      <input type="text" placeholder="email" value="{{email::input}}"></input> <br>
      <input type="password" placeholder="password" value="{{password::input}}"></input> <br><br>
      <button class="btn btn-outline-dark" on-click="login">login</button>
</div>



<span hidden$="{{!isLogged}}"> Bienvenido/a amigo</span>

      <iron-ajax
      id="doLogin"
      url="http://localhost:3000/apitechu/v2/login"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      email: {
        type: String, value :'1234@mail.com'
      }, password: {
        type: String , value : '1234'
      }, isLogged: {
        type: Boolean,
        value: false
      }
    };
  }// end properties

  login (){
    console.log("user press onlick");
    console.log("send the request");

    //montamos el body donde van los parametros que enviamos al POST
    var loginData = {
      "email" : this.email,
      "password" : this.password
    }

    console.log(loginData); //no se pone texto porque es object sino no se visualiza
    this.$.doLogin.body = JSON.stringify(loginData); //para accder a la función ajax this.$ para acceder a elemento local
    this.$.doLogin.generateRequest(); //esto es para ejecutar la función.

    console.log("petición enviada");


  }

  //captura el error definido en el iron ajax
  showError(error)
  {
    console.log("algo ha ido mal amigo");
    console.log(error);

  }

  //captura la respuesta del iron ajax
  manageAJAXResponse(data) {
    console.log("showDmanageAJAXResponseata");
    console.log(data.detail.response);
    console.log(data.detail.response.idUsuario);
    this.isLogged = true;

    this.sendEvent(data.detail.response);

    // this.email = data.detail.response.email;
    // this.password = data.detail.response.email;

  }

  sendEvent(e){
    console.log("has recuperado el id");
    //e es un parámetro que está en las funciones y tiene información del evento
    console.log(e);

    this.dispatchEvent(
      //clase predefinida con parámetros:
      //  - primero el nombre el que queramos [MINÚSCULAS siempre]
      //  - object que es lo que tiene dentro el evento "detail" con otro objeto dentro que tiene lo que queramos compartir
      new CustomEvent(
        "myevent",
        {
          "detail" : {
            "id" : e.idUsuario
          }
        }

      )
    )
  }




}//end class

window.customElements.define('login-usuario', LoginUsuario);
