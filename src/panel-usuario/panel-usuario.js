import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../login-usuario/login-usuario.js'
import '../visor-usuario/visor-usuario.js'

/**
 * @customElement
 * @polymer
 */
class PanelUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  
      <login-usuario on-myevent="processEvent"></login-usuario>

      <visor-usuario id="receiver"></visor-usuario>




  `;
  }

  static get properties() {
    return {
    };
  }// end properties

  processEvent(e){
    console.log("capturado el evento del emisor");
    console.log(e.detail);
    this.$.receiver.id=e.detail.id;
    this.$.receiver.show=true;


  }




}//end class

window.customElements.define('panel-usuario', PanelUsuario);
