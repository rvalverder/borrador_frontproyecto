import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../visor-account/visor-account.js'

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .redbg {
          background-color: red;
        }

        .greenbg {
          background-color: green;
        }

        .bluebg {
          background-color: blue;
        }

        .greybg {
          background-color: grey;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <div hidden$="{{!show}}">
      <h2>Bienvenido  [[first_name]] [[last_name]]</h2>
      <h3>tu correo es: [[email]]</h3>
      <button class="btn btn-primary btn-lg" on-click="verCuentas"   >ver cuentas </button>
    </div>
<!--      <div class="row greybg">
        <div class="col-2 offset-1 col-sm-6 redbg"> Col1 </div>
        <div class="col-3 offset-2 col-sm-1 greenbg"> Col2 </div> --><!--ofset deja columnas vacias junto a la col -->
<!--        <div class="col-4 col-sm-1 bluebg"> Col3 </div>

      </div>

      <button class="btn btn-primary btn-lg">primary </button>
      <button class="btn btn-secundary btn-sm">secundary </button>
      <button class="btn btn-success">success </button>
      <button class="btn btn-warning">warning </button>
      <button class="btn btn-info">info </button>
      <button class="btn btn-danger">danger </button>
      <button class="btn btn-light">light </button>
      <button class="btn btn-dark">dark </button>
      <button class="btn btn-outline-dark">Logout </button>
-->

<visor-account id="receiverAcc" ></visor-acount>


      <iron-ajax

      id="getUser"
      url="http://localhost:3000/apitechu/v2/users/{{id}}"
      handle-as="json"
      on-response="showData"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      }, last_name: {
        type: String
      }, email: {
        type: String
      }, id: {
        type: Number,
        observer : "_courseChanged"
      }, show : {
        type : Boolean,
        value : false
      }
    };
  }// end properties


  _courseChanged(newValue, oldValue) {
    console.log("Course value has changed" + newValue + "||" + oldValue);
    if (newValue)
      {
        this.$.getUser.generateRequest();
      }

  }


  verCuentas (){
    console.log("ver cuentas iduser");
    console.log("send the request");


    this.$.receiverAcc.iduser=this.id;
    this.$.receiverAcc.mostrar=true;


  }


  showData(data) {
    console.log("showData");
    console.log(data.detail.response);
    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;


  }

}//end class

window.customElements.define('visor-usuario', VisorUsuario);
