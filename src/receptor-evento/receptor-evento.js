import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class ReceptorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


      <h3>Soy el receptor</h3>
      <h4>Curso: [[course]]</h4>
      <h4>Año: [[year]]</h4>
      <input type="text" value="{{course::input}}"> </input> <!-- propiedad :: evento que tiene que escuchar en este caso input-->


  `;
  }

  static get properties() {
    return {
      course : {
        type : String,
        observer : "_courseChanged"
      }, year : {
        type : String
      }
    };
    }// end properties

    _courseChanged(newValue, oldValue) {
      console.log("Course value has changed");
      console.log("Old value " + oldValue);
      console.log("New value " + newValue);

    }



}//end class

window.customElements.define('receptor-evento', ReceptorEvento);
