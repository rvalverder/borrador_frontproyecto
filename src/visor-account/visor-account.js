import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */



class VisorAccount extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }

        .bluebg {
          background-color: blue;
        }

        .whitebg {
          background-color: white;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div hidden$="{{!mostrar}}">
      <h3>Listado de cuentas </h3>
      <div class="row whitebg">
        <div class="col-3 bluebg"> IBAN </div>
        <div class="col-1 bluebg"> BALANCE </div>
      </div>

      <dom-repeat items="{{accounts}}">

      <template>
        <div class="row whitebg">
         <div class="col-3"><span>{{item.IBAN}}</span></div>
         <div class="col-1"><span>{{item.balance}}</span></div>
        </div>
      </template>
      </dom-repeat>
</div>






      <iron-ajax

      id="getAccount"
      url="http://localhost:3000/apitechu/account/{{iduser}}"
      handle-as="json"
      on-response="showData"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      iban: {
        type: String
      }, iduser: {
        type: String,
        observer : "_courseChanged"
      }, balance: {
        type: Number
      }, accounts: {
        type: Array
      }, mostrar: {
        type: Boolean,
        value: false
      }
    };
  }// end properties





  _courseChanged(newValue, oldValue) {
    console.log("Account ha cambiado el id" + newValue + "||" + oldValue);
    if (newValue)
      {
        this.$.getAccount.generateRequest();}
  }


  showData(data) {
    console.log("showData");
    console.log(data.detail.response);

    this.iban = data.detail.response[0].IBAN;
    this.iduser = data.detail.response[0].userId;
    this.balance = data.detail.response[0].balance;

    this.accounts = data.detail.response;

  }

}//end class

window.customElements.define('visor-account', VisorAccount);
