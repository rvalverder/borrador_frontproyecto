import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class EmisorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          //all: initial; //con esto hacemos que no se propague los etilos del padre, que perduren los definidos en el componente.
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


      <h3>Emisor Evento</h3>

      <button class="btn btn-outline-dark" on-click="sendEvent">NO PULSAR</button>

  `;
  }

  static get properties() {
    return {
    };
  }// end properties

  sendEvent(e){
    console.log("El usuario ha pulsado el botón");

    //e es un parámetro que está en las funciones y tiene información del evento
    console.log(e);

    this.dispatchEvent(
      //clase predefinida con parámetros:
      //  - primero el nombre el que queramos [MINÚSCULAS siempre]
      //  - object que es lo que tiene dentro el evento "detail" con otro objeto dentro que tiene lo que queramos compartir
      new CustomEvent(
        "myevent",
        {
          "detail" : {
            "course" : "TechU",
            "year" : 2019
          }
        }

      )
    )
  }


}//end class

window.customElements.define('emisor-evento', EmisorEvento);
